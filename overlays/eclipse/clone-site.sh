#!/usr/bin/env bash

NAME=${1?Provide a package name}
SITE=${2?Provide a site path}

TMPDIR=$(mktemp -d 'siteclone.XXXXX')
ORIGDIR=$(pwd)
cd ${TMPDIR}

eclipse \
    -application org.eclipse.equinox.p2.artifact.repository.mirrorApplication \
    -source ${SITE} \
    -destination ${NAME}

zip -r ${NAME}.zip ${NAME}

