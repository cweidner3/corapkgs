self: super:
let
  base-eclipse = super.eclipses.eclipse-cpp;

  cppstyle = super.eclipses.plugins.buildEclipseUpdateSite rec {
    name = "cppstyle-${version}";
    version = "2018-12-03";

    src = ./cppstyle-site.zip;

    meta = with super.lib; {
      homepage = "http://www.cppstyle.com/";
      description = "C++ Style helper";
      license = licenses.mit;
      platforms = platforms.all;
    };
  };
in
{
  eclipse-w-pl = with super.eclipses; eclipseWithPlugins {
    eclipse = base-eclipse;
    jvmArgs = [ "-Xmx2048m" ];
    plugins = [
      plugins.vrapper
      cppstyle
    ];
  };

  eclipse-nix-builder = super.stdenvNoCC.mkDerivation rec {
    name = "eclipse-nix-builder";

    src = ./nix-shell-builder;

    dontUnpack = true;

    installPhase = ''
      mkdir -p $out/bin
      install $src $out/bin/nix-shell-builder
    '';

  };

  eclipse = super.symlinkJoin rec {
    name = "eclipse-wrapped";
    paths = [
      "${self.eclipse-w-pl}"
      "${self.eclipse-nix-builder}"
    ];
  };
}
