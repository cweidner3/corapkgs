let
  pkgs = import <nixpkgs> {};

  texlive-collection = pkgs.texlive.combine {
    inherit (pkgs.texlive) scheme-basic latexmk beamer;
  };
in
pkgs.mkShell {
  buildInputs = [ texlive-collection ];
}
