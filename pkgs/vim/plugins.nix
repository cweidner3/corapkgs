self: pkgs:

rec {
  myPlugin = pkgs.vimUtils.buildVimPluginFrom2Nix {
    name = "vim-myplugin";
    src = ./vim-plugin;
  };

  clever-f = pkgs.vimUtils.buildVimPluginFrom2Nix {
    pname = "clever-f";
    version = "1.5";
    src = pkgs.fetchFromGitHub {
      owner = "rhysd";
      repo = "clever-f.vim";
      rev = "c1e4774a91799083dc48b4a99ca8e8ab9a6352ea";
      sha256 = "1k9gm38jbwp3kv7wim7rvpv2pk998r1ymlgz76bdipkngjb05rxr";
    };
  };

  vim-bitbake = pkgs.vimUtils.buildVimPluginFrom2Nix {
    pname = "vim-bitbake";
    version = "2020-09-14";
    src = pkgs.fetchFromGitHub {
      owner = "kergoth";
      repo = "vim-bitbake";
      rev = "6d4148c3d200265293040a18c2f772340566554b";
      sha256 = "108m3ajjjwmldw0grcrp8wyavqyk72jqbf653gd5gh18g394q4iq";
    };
  };

  vim-plugins = with pkgs.vimPlugins; [
    nerdtree tagbar
    indentLine
    vim-nix vim-markdown haskell-vim vim-elixir vim-toml vim-bitbake
    robotframework-vim
    multiple-cursors
    vim-surround
    vim-better-whitespace
    table-mode
    easy-align
    Jenkinsfile-vim-syntax
    vim-css-color
  ] ++ [
    myPlugin
    clever-f
  ];
}
