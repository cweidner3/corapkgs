set nocompatible
filetype indent plugin on

" For the special characters
scriptencoding utf-8
set encoding=utf-8

set exrc   " Allow local vim files
set secure " Prevent autocmd in local vim files

if has('mouse')
  set mouse=a
endif

syntax enable
"set t_Co=256 " If avaliable

set backspace=indent,eol,start
set ruler
set number
set showcmd
set incsearch
set modeline

set ignorecase
set smartcase
set colorcolumn=80
set lcs=tab:\·\·,trail:#
set cursorline
set nowrap

set autoindent
set hlsearch
set shiftwidth=4
set tabstop=4
set expandtab
set nojoinspaces " disables two spaces after period when using gq
set foldlevel=99 " for when we turn on folding

" == Set Local Leader

nnoremap <SPACE> <Nop>
let mapleader=' '

" == Filetype detection overrides

au BufNewFile,BufRead *.resource set filetype=robot

" == Custom Commands

" Clear trailing spaces
command -nargs=0 Cts %s/\s\+$//e

" == NT & TB Key Mapping
" See :help NERDTree.txt
" See :help tagbar.txt

nnoremap <F2> :NERDTreeToggle<CR>
vnoremap <F2> <ESC>:NERDTreeToggle<CR>
inoremap <F2> <ESC>:NERDTreeToggle<CR>

nnoremap <F3> :TagbarToggle<CR>
vnoremap <F3> <ESC>:TagbarToggle<CR>
inoremap <F3> <ESC>:TagbarToggle<CR>

" == Indent Line
" See :help indentLine.txt

let g:indentLine_setColors=1
let g:indentLine_concealcursor='nv' " (n) Normal; (v) Visual; (i) Insert; (c) CmdLine
let g:indentLine_conceallevel=2

" == Better Whitespace
" See :help better-whitespace.txt

let g:better_whitespace_enabled=1
let g:strip_whitespace_on_save=1

" To disable auto-strip for these filetypes
let g:better_whitespace_filetypes_blacklist=[
	\ 'diff', 'gitcommit', 'unite', 'qf', 'help', 'markdown'
	\]

" == Clever F
" See :help clever-f.txt

" (1) f and F always move in their appropriate direction
" (0) f Continues in starting direction, F reverses course
let g:clever_f_fix_key_direction=0

" == Markdown Plugin
" See :help vim-markdown

let g:vim_markdown_folding_disabled=1
let g:vim_markdown_auto_insert_bullets=0

" == Haskell
" See :help haskell-vim.txt

let g:haskell_enable_quantification=1
let g:haskell_enable_recursivedo=1
let g:haskell_enable_arrowsyntax=1
let g:haskell_enable_pattern_synonyms=1
let g:haskell_enable_typeroles=1
let g:haskell_enable_static_pointers=1

let g:haskell_classic_highlighting=0
let g:haskell_disable_TH=0

let g:haskell_vim_indent_if=2
let g:haskell_vim_indent_case=2
let g:haskell_vim_indent_let=2
let g:haskell_vim_indent_where=2
let g:haskell_vim_indent_before_where=2
let g:haskell_vim_indent_after_bare_where=2
let g:haskell_vim_indent_do=2
let g:haskell_vim_indent_in=2
let g:haskell_vim_indent_guard=2

let g:cabal_indent_section=2

" == Easy Align

xmap <leader>aa <Plug>(EasyAlign)
nmap <leader>aa <Plug>(EasyAlign)

xnoremap <leader>as :EasyAlign /\s\s\+/<CR>
nnoremap <leader>as :EasyAlign /\s\s\+/<CR>

" == Table Mode

" Show current status of these globals
command -nargs=0 TMstatus
    \ let g:table_mode_separator
    \ | let g:table_mode_corner
    \ | let g:table_mode_corner_corner
    \ | let g:table_mode_header_fillchar
    \ | let g:table_mode_fillchar
    \ | let g:table_mode_map_prefix
    \ | let b:table_mode_corner
    \ | let b:table_mode_corner_corner
    \ | let b:table_mode_header_fillchar
" Command for TableMode Emacs Style
command -nargs=0 TMem
    \ let g:table_mode_separator="|"
    \ | let g:table_mode_corner='+'
    \ | let g:table_mode_corner_corner='+'
    \ | let g:table_mode_header_fillchar='='
    \ | unlet b:table_mode_corner
    \ | unlet b:table_mode_corner_corner
    \ | unlet b:table_mode_header_fillchar
" Command for TableMode Bitbucket Markdown Style
command -nargs=0 TMbb
    \ let g:table_mode_separator="|"
    \ | let g:table_mode_corner='|'
    \ | let g:table_mode_corner_corner='|'
    \ | let g:table_mode_header_fillchar='-'
    \ | unlet b:table_mode_corner
    \ | unlet b:table_mode_corner_corner
    \ | unlet b:table_mode_header_fillchar

" Default mode for table mode
"
"let g:table_mode_separator="|"
"let g:table_mode_corner='+'
"let g:table_mode_corner_corner='+'
"let g:table_mode_header_fillchar='='
