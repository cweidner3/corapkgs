" Colorscheme
colorscheme gruvbox

" == VIMTEX

" Specify that we want to handle latex vs tex
let g:tex_flavor = 'latex'
"let g:tex_conceal = ''
"let g:vimtex_fold_manual = 1
"let g:vimtex_latexmk_continuous = 1
"let g:vimtex_compiler_progname = 'nvr'
"" use SumatraPDF if you are on Windows
"let g:vimtex_view_method = 'skim'



" == COC.NVIM Configuration

" Settings to ensure coc works well
set hidden " Required so things don't fail
set nobackup " Some servers have issues with backup files, see #649
set nowritebackup " Some servers have issues with backup files, see #649
set cmdheight=2 " for displaying messages
set updatetime=300 " Speed things up a bit
set shortmess+=c " don't give \|ins-completion-menu\| messages.
set signcolumn=yes " always show signcolumns

" Key mappings

" Allow the use of TAB to cycle through list
"inoremap <silent><expr> <TAB> deoplete#complete()
inoremap <silent><expr> <TAB>
	\ pumvisible() ? "\<C-n>" :
	\ <SID>check_back_space() ? "\<TAB>" :
	\ coc#refresh()
inoremap <silent><expr> <S-TAB>
	\ pumvisible() ? "\<C-p>" :
	\ <SID>check_back_space() ? "\<TAB>" :
	\ coc#refresh()
function! s:check_back_space() abort
	let col = col('.') - 1
	return !col || getline('.')[col - 1]  =~ '\s'
endfunction

" Add manual code completion
inoremap <silent><expr> <c-space> coc#refresh()

" Diagnostics Navigation
nmap <silent> [c <Plug>(coc-diagnostic-prev)
nmap <silent> ]c <Plug>(coc-diagnostic-next)
" Remap keys for gotos
"nmap <silent> gd <Plug>(coc-definition)
"nmap <silent> gy <Plug>(coc-type-definition)
"nmap <silent> gi <Plug>(coc-implementation)
"nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window
"nnoremap <silent> K :call <SID>show_documentation()<CR>
"function! s:show_documentation()
"  if (index(['vim','help'], &filetype) >= 0)
"    execute 'h '.expand('<cword>')
"  else
"    call CocAction('doHover')
"  endif
"endfunction

" Highlight symbol under cursor on CursorHold
"autocmd CursorHold * silent call CocActionAsync('highlight')

" Remap for rename current word
"nmap <leader>rn <Plug>(coc-rename)

" Remap for format selected region
"xmap <leader>f  <Plug>(coc-format-selected)
"nmap <leader>f  <Plug>(coc-format-selected)

"augroup mygroup
"  autocmd!
"  " Setup formatexpr specified filetype(s).
"  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
"  " Update signature help on jump placeholder
"  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
"augroup end

" Remap for do codeAction of selected region, ex: `<leader>aap` for current paragraph
xmap <leader>l  <Plug>(coc-codeaction-selected)
nmap <leader>l  <Plug>(coc-codeaction-selected)
" Remap for do codeAction of current line
nmap <leader>lc  <Plug>(coc-codeaction)
"" Fix autofix problem of current line
"nmap <leader>qf  <Plug>(coc-fix-current)

" Use `:Format` to format current buffer
"command! -nargs=0 Format :call CocAction('format')

