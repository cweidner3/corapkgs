# vim: sw=2 ts=2 expandtab:
{ pkgs, symlinkJoin, pyPkgs ? pkgs.python38Packages, vimUtils, coc-omnisharp }:

let
  plugins = (import ./plugins.nix pkgs pkgs);

  vimrc = builtins.readFile ./common.vim;

  # To provide into neovim to add these to the python environment neovim will
  # use.
  extraPyPackages = py: with py; [
    # Deoplete Dependencies
    pynvim
    # Python LSP
    jedi   # Autocompletion
    pylint # Linter
    #python-language-server
    rope   # Refactor
  ];

  coc-omnisharp-vim = vimUtils.buildVimPluginFrom2Nix {
    pname = "coc-omnisharp";
    version = "2020-07-17";
    src = "${coc-omnisharp}/lib/node_modules/coc-omnisharp";
    meta.homepage = "https://github.com/coc-extensions/coc-omnisharp/";
  };

  instant-rst = pkgs.vimUtils.buildVimPluginFrom2Nix {
    pname = "instant-rst";
    version = "2020-03-13";
    src = pkgs.fetchFromGitHub {
      owner = "gu-fan";
      repo = "InstantRst";
      rev = "52144e8adb307db2c101432d2b90d84fb4255f61";
      sha256 = "1ngkyn3d0nfb68rkzwvd7vqr9a6rmm8ppk0qhihm5m5xsmh9f64b";
    };
  };

  neovim-configured = pkgs.neovim.override {
    extraPython3Packages = extraPyPackages;
    configure = {
      customRC = builtins.concatStringsSep ''''\n'' [
        (vimrc)
        (builtins.readFile ./neovim.vim)
      ];
      packages.myVimPackage = {
        start = [
          pkgs.vimPlugins.gruvbox # Colorscheme
        ]
        ++ plugins.vim-plugins
        ++ (with pkgs.vimPlugins; [
          coc-nvim
          coc-python
          coc-java
          coc-vimtex
          coc-yaml
          coc-omnisharp-vim
          coc-json
          coc-tslint
          coc-tsserver
          coc-rls
          coc-rust-analyzer

          instant-rst
          vimtex
        ]);
      };
    };
  };

in
symlinkJoin rec {
  name = "neovim-wrapped";
  paths = with pkgs; [
    neovim-configured
    # Common between
    (ctags.overrideAttrs(old: { meta.priority = "10"; }))
    # C/C++ LSP
    llvmPackages.clang-unwrapped
    ccls
    # C# LSP
    omnisharp-roslyn
    # Latex - Note: texlive needs to be installed on a per shell basis.
    procps

    nodejs
  ];
  meta.priority = "10";
}
