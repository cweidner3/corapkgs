let
  pkgs = import <nixpkgs> {};
  vim = pkgs.callPackage ./default.nix {};
  coc-omnisharp = (pkgs.callPackage ../node-packages {}).coc-omnisharp;
  neovim = pkgs.callPackage ./neovim.nix { coc-omnisharp = coc-omnisharp; };
in
pkgs.mkShell {
  buildInputs = with pkgs; [
    vim
    neovim
  ];
}
