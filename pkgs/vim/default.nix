# vim: sw=2 ts=2 expandtab:
{ pkgs, symlinkJoin }:

let
  plugins = (import ./plugins.nix pkgs pkgs);

  vimrc = builtins.readFile ./common.vim;

  vim-configured = pkgs.vim_configurable.customize {
    name = "vim";
    vimrcConfig = {
      customRC = builtins.concatStringsSep ''''\n'' [
        (vimrc)
        # The extra spaces are needed
        ''

          colorscheme NeoSolarized
          set background=dark
          let g:neosolarized_contrast = "high"
          let g:neosolarized_visibility = "high"
          let g:neosolarized_vertSplitBgTrans = 1
          let g:neosolarized_bold = 1
          let g:neosolarized_underline = 1
          let g:neosolarized_italic = 0
          let g:neosolarized_termBoldAsBright = 1

        ''
      ];
      packages.myVimPackage = {
        start = [
          pkgs.vimPlugins.NeoSolarized # Colorscheme
        ] ++ plugins.vim-plugins;
      };
    };
  };

in
symlinkJoin rec {
  name = "vim-wrapped";
  paths = with pkgs; [
    vim-configured
    # Common between
    ctags
  ];
}
