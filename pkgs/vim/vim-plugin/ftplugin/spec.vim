setlocal expandtab       " Use spaces instead of tab
setlocal tabstop=4       " Tab is 4 characters (standard for kernel work is 8)
setlocal shiftwidth=4    " Number of spaces to use for autoindenting
setlocal softtabstop=4   " Backspaces will delete a tab worth of spaces

setlocal colorcolumn=80  " Display a column line for manual wordwrap

