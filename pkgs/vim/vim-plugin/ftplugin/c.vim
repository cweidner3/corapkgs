" Tabs
setlocal expandtab       " Use spaces instead of tab
setlocal tabstop=4       " Tab is 4 characters (standard for kernel work is 8)
setlocal shiftwidth=4    " Number of spaces to use for autoindenting

" Folding
setlocal foldmethod=syntax " How to determine folds

" EOF
