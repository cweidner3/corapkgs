{ pkgs }:

let
  fmt-all = pkgs.fmt.overrideAttrs(old: { outputs = [ "out" ]; });
in
pkgs.stdenv.mkDerivation rec {
  pname = "tea-time";
  version = "0.1.0";

  src = pkgs.fetchFromGitHub {
    owner = "cweidner3";
    repo = "tea-time";
    rev = "v${version}";
    sha256 = "14riy8pdr3f8jdqcfs5h0fxqkagq1pq1rlqhah6spk6q4v81y18d";
  };

  buildInputs = with pkgs; [
    fmt-all
    gcc
    boost.all
    qt5.qtbase
  ];

  nativeBuildInputs = with pkgs; [
    git
    cmake
    pkgconfig
    pandoc
    qt5.wrapQtAppsHook
  ];

  postFixup = ''
    sed -i 's~^Exec=/usr/bin/~Exec=~' $out/share/applications/tea-time.desktop
  '';
}
