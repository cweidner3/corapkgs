{ pkgs ? import <nixpkgs> {} }:

let
  inherit (pkgs) callPackage;
in
rec {
  #pkgAttrName = callPackage ./pkgs/pkgPath {};

  node2nix = pkgs.nodePackages.node2nix;
  nodePackages = callPackage ./node-packages {};

  vim = callPackage ./vim {};

  neovim = callPackage ./vim/neovim.nix { coc-omnisharp = nodePackages.coc-omnisharp; };

  sweet-folders = callPackage ./sweet-folders {};

  candy-icons = callPackage ./candy-icons {};

  tea-time = callPackage ./tea-time {};

  i3lock = callPackage ./i3lock { pam = rhel-pam; };

  rhel-pam = callPackage ./rhel-pam {};

  pamtester = callPackage ./pamtester { pam = rhel-pam; };
}
