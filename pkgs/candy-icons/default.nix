{ lib, pkgs, stdenv, fetchFromGitHub }:
stdenv.mkDerivation rec {
  pname = "candy-icons";
  version = "2020.5.14";
  _rev = "9d5b05d0fb93d84387876d7f7d16f4bdad6bdf6f";
  _owner = "EliverLara";
  _repo = "candy-icons";

  src = builtins.fetchurl {
    url = "https://github.com/${_owner}/${_repo}/archive/${_rev}.tar.gz";
    sha256 = "15axr6y8510l6amyy8l2m2ank85rgc5bxjk8zmdqd10lh0l3nrgn";
  };

  unpackPhase = false;

  # TODO determine how to fix the links
  installPhase = ''
    mkdir -p $out/share/icons
    cd $out/share/icons
    tar -xf $src \
      --wildcards \
      -C $out/share/icons \
      \*/apps \*/places \*/index.theme
    mv ${pname}* ${pname}
  '';

  meta = with lib; {
    description = "Candy Icon theme to go along with the Sweet GTK+ themes";
    homepage = "https://github.com/EliverLara/sweet-folders";
    license = licenses.gpl3;
    maintainers = with maintainers; [ fuzen ];
    platforms = platforms.linux;
  };
}
