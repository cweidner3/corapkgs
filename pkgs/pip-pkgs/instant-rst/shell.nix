{ pkgs ? import <nixpkgs> {} }:
let
  instant-rst = pkgs.callPackage ./default.nix {};
  pyenv = pkgs.python3.withPackages (ps: with ps; [instant-rst]);
in
pkgs.mkShell {
  buildInputs = with pkgs; [
    pyenv
  ];
}
