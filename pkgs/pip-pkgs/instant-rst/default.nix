{ pkgs
, buildPythonPackage ? pkgs.python3Packages.buildPythonPackage
, fetchFromGitHub
, flask
}:

buildPythonPackage {
  version = "2020-04-03";
  pname = "instant-rst";

  src = pkgs.fetchFromGitHub {
    owner = "gu-fan";
    repo = "instant-rst.py";
    rev = "305be3a7e10481a546f5802ec2e5e53d450a8a18";
    sha256 = "1mbzmr3y76wiqdvdz378375k5060kl3d5rb3j85k5mxr4d7lb90w";
  };

  propagatedBuildInputs = [ flask ];

  meta = with pkgs.lib; {
    description = "Server-side code for InstantRst vim plugin";
    homepage = "https://github.com/gu-fan/InstantRst";
    license = licenses.mit;
    #maintainers = with maintainers; [ lihop ];
  };
}
