{ lib, fetchFromGitHub, stdenv, which, pkgconfig, libxcb, xcbutilkeysyms, xcbutilimage,
xcbutilxrm, pam, libX11, libev, cairo, libxkbcommon, libxkbfile, git, autoconf,
autobuild, autogen, automake, pam_ldap, nss_pam_ldapd, makeWrapper }:

stdenv.mkDerivation rec {
  pname = "i3lock";
  version = "2.12";

  src = fetchFromGitHub {
    owner = "i3";
    repo = "i3lock";
    rev = "${version}";
    sha256 = "02w2hk57q7afn42qmbk60miabcgrmfisy78lwav1v3v5zgaa1c1z";
  };

  #patch = ./i3lock.patch;

  nativeBuildInputs = [ pkgconfig autoconf autobuild autogen automake ];
  buildInputs = [ which libxcb xcbutilkeysyms xcbutilimage xcbutilxrm
  pam libX11 libev cairo libxkbcommon libxkbfile ]
  ++ [ pam_ldap nss_pam_ldapd makeWrapper];

  enableParallelBuilding = true;

  makeFlags = [ "all" ];
  #installFlags = [ "PREFIX=\${out}" "SYSCONFDIR=\${out}/etc" ];

  patchPhase = ''
    ${git}/bin/git apply ${./i3lock.patch}
  '';

  buildPhase = ''
    autoreconf --force --install
    ./configure \
        --prefix=$out \
        --sysconfdir=/etc \
        --disable-sanitizers
    make all
  '';

  postInstall = ''
    mkdir -p $out/share/man/man1
    cp *.1 $out/share/man/man1
  '';

  #fixupPhase = ''
  #  wrapProgram $out/bin/i3lock --set LD_LIBRARY_PATH ${pam}/lib-support
  #'';

  meta = with lib; {
    description = "A simple screen locker like slock";
    longDescription = ''
      Simple screen locker. After locking, a colored background (default: white) or
      a configurable image is shown, and a ring-shaped unlock-indicator gives feedback
      for every keystroke. After entering your password, the screen is unlocked again.
    '';
    homepage = "https://i3wm.org/i3lock/";
    maintainers = with maintainers; [ malyn domenkozar ];
    license = licenses.bsd3;
    platforms = platforms.all;
  };

}
