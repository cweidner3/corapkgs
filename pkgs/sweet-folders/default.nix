{ lib, pkgs, stdenv, fetchFromGitHub }:
stdenv.mkDerivation rec {
  pname = "sweet-folders";
  version = "2020.2.27";
  _rev = "d50fbe3d93df4c494958c773b681ab36049935a1";
  _owner = "EliverLara";
  _repo = "Sweet-folders";

  src = builtins.fetchurl {
    url = "https://github.com/${_owner}/${_repo}/archive/${_rev}.tar.gz";
    sha256 = "1bnkg5ylhb071w7p11bc3giwf650xhkb4srql7nfx1ab9wbghkzk";
  };

  unpackPhase = false;

  # TODO determine how to fix the links
  installPhase = ''
    mkdir -p $out/share/icons
    tar -xf $src -C $out/share/icons --strip-components=1
    rm $out/share/icons/{LICENSE,README*}
  '';

  meta = with lib; {
    description = "Icon theme to go along with the Sweet GTK+ themes";
    homepage = "https://github.com/EliverLara/sweet-folders";
    license = licenses.gpl3;
    maintainers = with maintainers; [ fuzen ];
    platforms = platforms.linux;
  };
}
